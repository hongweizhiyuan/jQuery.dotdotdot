$(document).ready(function() {

	var _w = $(window),
		_h = $('#header h1'),
		oT = true;

	_w.scroll(function() {
		var _s = _w.scrollTop();
		if (!oT && _s < 200) {
			oT = true;
			_h.addClass('on-top');
		} else if (oT && _s > 200) {
			oT = false;
			_h.removeClass('on-top');
		}
	}).scroll();
	
	_w.resize(function() {
		var bw = $('body').width();
		if (bw < 740) bw = 740;
		$('.ellipsis-r').css({
			width: bw / 1.5,
			marginLeft: (740 - (bw / 1.5)) / 2
		});

	}).resize();

	$('a.js').fancybox({
		overlayColor: '#fff'
	});

	$('.ellipsis, .ellipsis-e').dotdotdot({
		wrap: 'letter'
	});
	$('.ellipsis-r').dotdotdot({
		wrap: 'letter',
		watch: 'window'
	});
	$('.ellipsis-a').dotdotdot({
		wrap: 'letter',
		after: 'a.readmore'
	});

	$('.ellipsis-p .pathname').each(function() {
		var path = $(this).html().split( '/' );
		if ( path.length > 1 ) {
			var name = path.pop();
			$(this).html( path.join( '/' ) + '<span class="filename">/' + name + '</span>' );
			$(this).dotdotdot({
				after: '.filename',
				wrap: 'letter'
			});						
		}
	});

	var __i = 'info',
		__d = 'frebsite',
		__e = 'nl';
	$('#contact').attr('href', 'mailto:'+__i+'@'+__d+'.'+__e);
	
	prettyPrint();
});